<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'first_name' => 'Cris',
                'last_name' => 'Navarroza',
                'email' => 'admin@mail.com',
                'password' => bcrypt('hello123'),
                'branch_id' => '1',
            ],
            [
                'first_name' => 'Cashier',
                'last_name' => 'Cashier',
                'email' => 'cashier@mail.com',
                'password' => bcrypt('hello123'),
                'branch_id' => '1',
            ]
            
        ]);
    }
}
