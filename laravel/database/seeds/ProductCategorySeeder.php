<?php

use Illuminate\Database\Seeder;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_category')->insert([
            [
                'id' => 1,
                'name' => 'Breakfast',
                'with_sizes' => false
            ],
            [
                'id' => 2,
                'name' => 'Snacks',
                'with_sizes' => false
            ],
            [
                'id' => 3,
                'name' => 'Dinner',
                'with_sizes' => false
            ],
            [
                'id' => 4,
                'name' => 'Lunch',
                'with_sizes' => false
            ],
            [
                'id' => 5,
                'name' => 'Milktea',
                'with_sizes' => true
            ],
        ]);
    }
}
