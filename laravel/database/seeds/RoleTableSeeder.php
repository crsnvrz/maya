<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'admin',
                'description' => 'All Access',
            ],
            [
                'name' => 'cashier',
                'description' => 'Secretary Access',
            ]
        ]);

        DB::table('role_user')->insert([
            [
                'user_id' => '1',
                'role_id' => '1',
            ],
            [
                'user_id' => '2',
                'role_id' => '2',
            ]
        ]);
    }
}
