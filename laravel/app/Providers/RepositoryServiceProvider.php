<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Contracts\Media::class, \App\Repositories\MediaRepository::class);

        $this->app->bind(\App\Contracts\Product::class, \App\Repositories\ProductRepository::class);

        $this->app->bind(\App\Contracts\User::class, \App\Repositories\UserRepository::class);
    }
}
