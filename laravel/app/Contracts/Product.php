<?php

namespace App\Contracts;

interface Product {
    public function store($request);
    public function update($request, $product);
}