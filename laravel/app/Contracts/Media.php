<?php

namespace App\Contracts;

interface Media {
    public function savePhoto($cover_image);
    public function saveSize($cover_image, $size);
    public function saveMedia($name, $post, $size);
}