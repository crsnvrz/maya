<?php

namespace App\Contracts;

interface User {
    public function getUser();
    public function search($request);
    public function branchRoles();
    public function store($request);
}