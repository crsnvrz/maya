<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = ['name', 'description', 'width'];

    public function mediable()
    {
      return $this->morphTo();
    }
}
