<?php

namespace App\Repositories;

use App\Contracts\Product as ProductInterface;
use App\Contracts\Media as MediaInterface;
use App\Models\Branch;
use App\Models\Product;
use App\Models\ProductCategory;
use Image;

class ProductRepository implements ProductInterface
{
    protected $media;

    public function __construct(MediaInterface $media){
        $this->media = $media;
    }

    public function store($request)
    {
        $product = new Product($request->all());
        $product->save();

        if ($request->hasFile('cover_image')) {
            $this->coverImage = $request->file('cover_image');
            $this->blog = $product;
            $fileName = $this->media->savePhoto($this->coverImage);
            $width = Image::make($this->coverImage)->width();
            $this->media->saveMedia($fileName, $this->blog, $width);

            $this->sizes = collect( $this->sizes);
            $this->sizes->each(function($size){
                $resizedImg = $this->media->saveSize($this->coverImage, $size);
                $this->media->saveMedia($resizedImg, $this->blog, $size);
            });
        } 
    }

    public function update($request, $product)
    {
        $product->fill($request->all());
        
        $this->blog = $product;

        $product->save();

        $images = $product->media()->where('mediable_id', $product->id)->pluck('name');

        if ($request->hasFile('cover_image')) {
            $images->each(function($image){
                if($image != 'noimage.png'){            
                    Storage::delete('public/cover_images/'.$image);
                }
                $this->blog->media()->delete();
            });

            $this->coverImage = $request->file('cover_image');
            $this->blog = $product;
            $fileName = $this->media->savePhoto($this->coverImage);
            $width = Image::make($this->coverImage)->width();
            $this->media->saveMedia($fileName, $this->blog, $width);

            $this->sizes = collect($this->sizes);
            $this->sizes->each(function($size){
                $resizedImg = $media->saveSize($this->coverImage, $size);
                $this->media->saveMedia($resizedImg, $this->blog, $size);
            });
        }
    }

    public function removeImages($product) 
    {
        $images = $product->media()->where('mediable_id', $product->id)->pluck('name');

        $images->each(function($image){
            if($image != 'noimage.png'){            
                Storage::delete('public/cover_images/'.$image);
            }
        });
    }

    public function categoryBranch() 
    {
        $categories = ProductCategory::all()->pluck('name', 'id');
        $branches = Branch::all()->pluck('name', 'id');

        return compact('categories', 'branches');
    }
}