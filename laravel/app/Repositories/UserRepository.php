<?php

namespace App\Repositories;

use App\Contracts\User as UserInterface;
use App\Models\Branch;
use App\Models\Role;
use App\Models\User;
use Hash;

class UserRepository implements UserInterface
{
    public function getUser()
    {
        $users = User::whereHas("roles", function($query) {
            $query->where("name", '!=', 'admin');
        })->paginate(5);

        return $users;
    }

    public function search($request)
    {
        $request->validate([
            'search' => 'required',
        ]);

        $users = User::where(function($query) use ($request) {
            $query->where('first_name', 'like', "%{$request->search}%")
            ->orWhere('last_name', 'like', "%{$request->search}%")
            ->orWhere('email', 'like', "%{$request->search}%");
        })
        ->paginate(5);

        return $users;
    }

    public function branchRoles()
    {
        $branches = Branch::all()->pluck('name', 'id');
        $roles = Role::where('name', '!=', 'dentist')->pluck('name', 'id');

        return compact('roles', 'branches');
    }

    public function store($request)
    {
        $user = new User();
        $user->fill($request->all());
        $user->password = Hash::make($request->password);
        $user->save();
        $user->roles()->attach($request->role);
    }
}