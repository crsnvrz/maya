<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Contracts\Media as MediaInterface;
use App\Http\Requests\ProductRequest;
use App\Contracts\Product as ProductInterface;

class ProductController extends Controller  
{
    protected $sizes  = [80, 160, 240, 480, 800, 1080];
    protected $coverImage, $blog, $media, $product;

    private $category = 1;

    public function __construct(MediaInterface $media, ProductInterface $product) 
    {
        $this->media = $media;
        $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->has('category_id')){
            $this->category = request()->get('category_id');
        }

        $products = Product::whereHas('product_category', function($query){
            $query->where('id', $this->category);
        })
        ->paginate(5);

        $selected_category = $this->category;

        $categories = ProductCategory::all(); 
        
        return view('backend.pages.products.index', compact('products', 'categories', 'selected_category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoryBranch = $this->product->categoryBranch();
        return view('backend.pages.products.create', compact('categoryBranch'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $this->product->store($request);
        return redirect('products')->with('success', 'Product added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categoryBranch = $this->product->categoryBranch();
        return view('backend.pages.products.edit', compact('product', 'categoryBranch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        $this->update($request, $product);
        return redirect('/products')->with('success', 'Product updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->product->removeImages($product);
        
        try {
            $product->media()->delete();
            $product->delete();
            
            return redirect('/products')->with('success', 'Product Removed');

        } catch(Exception $exception)
        {
            $errormsg = 'Sorry, unable to delete product:'. $errormsg;
            return redirect('/products')->with('error', $errormsg);
        } 
    }
}
