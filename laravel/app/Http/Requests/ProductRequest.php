<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'product_category_id' => 'required',
            'price' => 'required|integer',
            'price_medium' => 'nullable|integer',
            'price_large' => 'nullable|integer',
            'cover_image' => 'image|nullable|max:1999',
            'quantity' => 'nullable|integer',
            'quantity_medium' => 'nullable|integer',
            'quantity_large' => 'nullable|integer'
        ];
    }
}
