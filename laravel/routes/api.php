<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::post('getItems', 'Api\ProductController@getItems')->name('getItems');

Route::post('saveOrders', 'Api\ProductController@saveOrders')->name('saveOrders');

Route::post('getCategory', 'Api\ProductController@getCategory')->name('getCategory');