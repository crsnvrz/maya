/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!************************************************!*\
  !*** ./resources/assets/js/custom/products.js ***!
  \************************************************/
var a = new Vue({
  el: '#app',
  data: {
    is_show: false
  },
  created: function created() {
    this.initCat();
  },
  methods: {
    selectCategory: function selectCategory() {
      var _this = this;
      var selected = $("#product_category_id").val();
      if (!selected) selected = 1;
      axios.post('/api/getCategory', {
        id: selected
      }).then(function (response) {
        if (response.data.with_sizes) {
          _this.is_show = true;
          return false;
        }
        _this.is_show = false;
      });
    },
    initCat: function initCat() {
      var _this2 = this;
      var selected = $("#product_category_id").val();
      axios.post('/api/getCategory', {
        id: selected
      }).then(function (response) {
        if (response.data.with_sizes) {
          _this2.is_show = true;
          return false;
        }
        _this2.is_show = false;
      });
    },
    chooseCategory: function chooseCategory() {
      var cat = $("#categories option:selected").val();
      $("input[name='category_id']").val(cat);
      $("#prodForm").submit();
    }
  }
});
$(function () {
  a.selectCategory();
});
/******/ })()
;