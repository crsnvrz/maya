@extends('backend.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">Add Branch</div>
    <div class="card-body">
            {!! Form::model($branch, ['route' => ['branches.update', $branch->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
            <div class="form-row">
                <div class="form-group col-5">
                    {!! Form::label('name', 'Branch Name') !!}
                    {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-5">
                    {!! Form::label('address', 'Address') !!}
                    {!! Form::text('address', null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <button type="submit" class ="btn btn-primary" onclick="blur()">Save</button>
            {!! Form::close() !!}
        </div>
    </div>
</div> 
@endsection 