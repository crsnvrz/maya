@extends('backend.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">Show Transaction</div>
    <div class="modal-body printDiv">
        <center>
            <img src="{{ asset('assets/images/maya-logo.jpg') }}" alt="" width="100px">
        <br><br>
        <table>
            <tbody class="table">
                @foreach($transaction->orders as $order)
                <tr>
                    <td>
                        {{ $order->product->name }}
                    </td>
                    <td>
                        {{ $order->quantity ." X ". $order->price}}
                    </td>
                    <td>
                        {{ $order->quantity * $order->price }}
                    </td>
                </tr>
                @endforeach
                <tr>
                    <td>
                        <b>Total</b>
                    </td>
                    <td colspan="2">
                        <span class="pull-right"> {{ $transaction->total_amount + $transaction->discount }} </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>12% VAT</b>
                    </td>
                    <td colspan="2">
                        <span class="pull-right"> {{ ($transaction->total_amount + $transaction->discount) * 0.12 }} </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>20% Discount</b>
                    </td>
                    <td colspan="2">
                        <span class="pull-right"> {{ ($transaction->total_amount + $transaction->discount) * 0.20 }} </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Total Amount</b>
                    </td>
                    <td colspan="2">
                        <span class="pull-right"> {{ $transaction->total_amount }} </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Cash</b>
                    </td>
                    <td colspan="2">
                        <span class="pull-right"> {{ $transaction->cash }} </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Change</b>
                    </td>
                    <td colspan="2">
                        <span class="pull-right"> {{ $transaction->change }} </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <center>
                            <button class="btn btn-success" v-on:click="printReceipt">Print Receipt</button>
                        </center>
                    </td>
                </tr>
            </tbody>
        </table>
        </center>
    </div>
</div> 
@endsection

@section('js')
<script src="{{ asset('js/cashier.js') }}"></script>
@endsection
