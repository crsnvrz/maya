@extends('backend.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">Update Product Inventory</div>
    <div class="card-body">
            {!! Form::model($product, ['route' => ['inventory.update', $product->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
            <div class="form-row">
                <div class="form-group col-4">
                    {!! Form::label('name', 'Product Name') !!}
                    {!! Form::text('name', null, ['class' => 'form-control', 'readonly']) !!}
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-4">
                    {!! Form::label('quantity', 'Quantity') !!}
                    {!! Form::text('quantity', null, ['class' => 'form-control', 'required', 'onkeypress' => 'return isNumber(event)']) !!}
                </div>
                @if($product->product_category->with_sizes)
                <div class="form-group col-4">
                    {!! Form::label('quantity_medium', 'Quantity (Medium)') !!}
                    {!! Form::text('quantity_medium', null, ['class' => 'form-control', 'onkeypress' => 'return isNumber(event)']) !!}
                </div>
                <div class="form-group col-4">
                    {!! Form::label('quantity_large', 'Quantity (Large)') !!}
                    {!! Form::text('quantity_large', $null, ['class' => 'form-control', 'onkeypress' => 'return isNumber(event)']) !!}
                </div>
                @endif
            </div>
            <button type="submit" class ="btn btn-primary" onclick="blur()">Save</button>
            {!! Form::close() !!}
        </div>
    </div>
</div> 
@endsection 
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
    $(".cat_sect").select2({
        placeholder : "Choose a tag",
        maximumSelectionLength: 1
    });
    </script>
@endsection