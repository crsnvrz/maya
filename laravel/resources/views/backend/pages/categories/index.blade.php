@extends('backend.layouts.app')

@section('css')
    <style>
        thead > tr > td{
            font-weight: bold;
        }
        tbody > tr > td > img{
            width: 40px;;
        }
    </style>
@endsection
@section('content')
<div class="card">
    <div class="card-header">Categories</div>
    <div class="card-body">
       <a href="{{ route('categories.create') }}" class="btn btn-primary" onclick="blur()">Add Category</a>
       <br><br>
       @if($categories->isNotEmpty())
       <div class="table">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>Category Name</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories as $category)
                    <tr>
                        <td>{{ $category->name }}</td>
                        <td>
                            <a href="{{route('categories.edit', $category->id)}}"><button type="button" class="btn btn-info">Edit</button></a>
                            {!!Form::open(['route' => ['categories.destroy', $category->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                            {{Form::hidden('_method', 'DELETE')}}
                            {{Form::submit('Delete', ['class' => 'btn btn-danger', 'onclick'=> 'return confirm("Are you sure you want to delete?")'])}}
                            {!!Form::close()!!}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @else
        <center>
            <h2>No categories yet...</h2>
        </center>
        @endif
    </div>
</div> 
@endsection
