@extends('backend.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">Add Category</div>
    <div class="card-body">
            {!! Form::model($category, ['route' => ['categories.update', $category->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
            <div class="form-row">
                <div class="form-group col">
                    {!! Form::label('name', 'Category Name') !!}
                    {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
                </div>
                <div class="col"></div>
            </div>
            <button type="submit" class ="btn btn-primary" onclick="blur()">Save</button>
            {!! Form::close() !!}
        </div>
    </div>
</div> 
@endsection 
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
    $(".cat_sect").select2({
        placeholder : "Choose a tag",
        maximumSelectionLength: 1
    });
    </script>
@endsection