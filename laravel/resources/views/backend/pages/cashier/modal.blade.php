<div class="modal" id="cashier-modal" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Quantity</h5>
    </div>
    <div class="modal-body">
        {{ Form::open(['id' => 'quantity-form', 'v-on:submit.prevent' => 'addQuantity']) }}
        <div class="form-group">
            {{ Form::text('quantity', null, ['class' => 'form-control', 'v-model' => 'order_quantity', 'onkeypress' => 'return isNumber(event)', 'maxlength' => '4']) }}
        </div>
        <div class="form-group" v-if="is_sizes">
            {!! Form::select('sizes', ['small' => 'Small', 'medium' => 'Medium', 'large' => 'Large'], null, ['class' => 'form-control', 'v-on:change' => 'setPrice($event)', 'required']) !!}
        </div>
        <button type="submit" class="btn btn-primary pull-right">ADD</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" v-if="show_close_modal" v-on:click="closeModal">Close</button>
        {!! Form::close() !!}
    </div>
    </div>
</div>
</div>


<div class="modal" id="receipt-modal" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Quantity</h5>
    </div>
    <div class="modal-body printDiv">
        <center>
            <img src="{{ asset('assets/images/maya-logo.jpg') }}" alt="" width="100px">
        <br><br>
        <table>
            <tbody class="table">
                <tr v-for="order in receipt">
                    <td>
                        @{{ order.product.name }}
                    </td>
                    <td>
                        @{{ order.quantity +" X "+ order.price.toFixed(2)}}
                    </td>
                    <td>
                        @{{ order.quantity * order.price }}
                    </td>
                </tr>
                <tr v-if="transaction">
                    <td>
                        <b>Total</b>
                    </td>
                    <td colspan="2">
                        <span class="pull-right"> @{{ transaction.total_amount + transaction.discount }} </span>
                    </td>
                </tr>
                <tr v-if="transaction">
                    <td>
                        <b>12% VAT</b>
                    </td>
                    <td colspan="2">
                        <span class="pull-right"> @{{ (transaction.total_amount + transaction.discount) * 0.12 }} </span>
                    </td>
                </tr>
                <tr v-if="transaction">
                    <td>
                        <b>20% Discount</b>
                    </td>
                    <td colspan="2">
                        <span class="pull-right"> @{{ (transaction.total_amount + transaction.discount) * 0.20 }} </span>
                    </td>
                </tr>
                <tr v-if="transaction">
                    <td>
                        <b>Total Amount</b>
                    </td>
                    <td colspan="2">
                        <span class="pull-right"> @{{ transaction.total_amount }} </span>
                    </td>
                </tr>
                <tr v-if="transaction">
                    <td>
                        <b>Cash</b>
                    </td>
                    <td colspan="2">
                        <span class="pull-right"> @{{ transaction.cash }} </span>
                    </td>
                </tr>
                <tr v-if="transaction">
                    <td>
                        <b>Change</b>
                    </td>
                    <td colspan="2">
                        <span class="pull-right"> @{{ transaction.change }} </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <center>
                            <button class="btn btn-success" v-on:click="printReceipt">Print Receipt</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </center>
                    </td>
                </tr>
            </tbody>
        </table>
        </center>
    </div>
    </div>
</div>
</div>

