@extends('backend.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <b>User Record</b>  
    </div>
    <div class="card-body">
      <a href="{{ route('users.create') }}" class="btn btn-primary btn-sm" onclick="blur()">Add User</a>
    <div class="pull-right">
        {{ Form::open(['route' => 'users.search', 'method' => 'GET', "enctype" => "multipart/form-data", 'id' => 'patient-form']) }}
        <div class="form-inline">
            {{ Form::text('search', null, ['class' => 'form-control', 'placeholder' => 'Search']) }}
            &nbsp;
            <button class="btn btn-success">Search</button>
            &nbsp;
            <a href="{{ route('users.index') }}" class="btn btn-info">View All</a>
        </div>
        {!! Form::close() !!}
    </div>
      <br><br>
      @if($users->isEmpty())
                <h1>No users...</h1>
      @else
      <table class="table">
          <thead>
              <tr>
                  <td><b>First Name</b></td>
                  <td><b>Last Name</b></td>
                  <td><b>Email</b></td>
                  <td><b>Role</b></td>
              </tr>
          </thead>
          <tbody>
              @foreach($users as $user)
              <tr> 
                    <td>
                        {{$user->first_name}}
                    </td>
                    <td>
                        {{$user->last_name}}
                    </td>
                    <td>
                        {{$user->email}}
                    </td>
                    <td>
                        {{$user->roles->first()->name}}
                    </td>
                    <td>
                        <div class="row">
                        {{ Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $user], 'class' => 'deleteForm']) }}
                        {{ Form::hidden('id', $user->id) }}
                            <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm">Delete</button>
                        {{ Form::close() }}
                    </td>
              </tr>     
              @endforeach
          </tbody>
      </table>
      @endif
      {{ $users->links() }}
    </div>
  </div>
@endsection
