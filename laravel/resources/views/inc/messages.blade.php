@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <div style="padding-left: initial;">
            <div class="alert alert-danger">
                {{$error}}
            </div>
        </div>
    @endforeach
@endif

@if(session('success'))
    <div style="padding-left: initial;">
        <div class="alert alert-success">
            {{session('success')}}
        </div>
    </div>
@endif

@if(session('error'))
    <div style="padding-left: initial;">
        <div class="alert alert-danger">
            {{session('error')}}
        </div>
    </div>
@endif