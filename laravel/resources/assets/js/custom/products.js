

var a = new Vue({
    el: '#app',
    data: {
        is_show: false,
    },
    created() {
        this.initCat()
    },
    methods:{
        selectCategory() {
            let selected = $("#product_category_id").val()
            if (!selected) selected = 1
            
            axios.post('/api/getCategory', {id: selected} )
                .then(response => {
                if(response.data.with_sizes) {
                    this.is_show = true
                    return false
                }
                this.is_show = false
            })
            
        },
        initCat() {
            let selected = $("#product_category_id").val()

            axios.post('/api/getCategory', {id: selected} )
                .then(response => {
                if(response.data.with_sizes) {
                    this.is_show = true
                    return false
                }
                this.is_show = false
            })
        },
        chooseCategory() {
            var cat = $("#categories option:selected").val()
            $("input[name='category_id']").val(cat)
            $("#prodForm").submit()
        }
    },
});

$(function() {
    a.selectCategory()
})